test:
	@pytest --cov --cov-report term-missing

reset_test_db:
	@dropdb test_auth; createdb test_auth

reset_db:
	@dropdb auth; createdb auth

static-analysis:
	@bandit -r src || true
	@black --check src tests || true
	@flake8 --statistics src tests || true
	@mypy src tests || true
	@pylint src tests || true
