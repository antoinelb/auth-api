from typing import Any, Dict

from argon2 import PasswordHasher

from src.db import User


async def test_user__str(user: User, admin: User) -> None:
    assert str(user) == f"<user {user.username}>"
    assert str(admin) == f"<admin user {admin.username}>"


async def test_user__dict(user: User) -> None:
    user_: Dict[str, Any] = dict(user)
    for field in (
        "id",
        "username",
        "is_admin",
        "pin",
        "created_on",
        "last_modified",
    ):
        assert user_[field] == getattr(user, field)
    assert len(user_) == 7


async def test_user__info(user: User) -> None:
    assert user.info["username"] == user.username
    assert user.info["is_admin"] == user.is_admin
    assert user.info["pin"] == user.pin
    assert user.info["created_on"] == user.created_on
    assert user.info["last_modified"] == user.last_modified
    assert len(user.info) == 5


async def test_user_pre_save() -> None:
    user = await User.create(username="test", password="test")
    assert user.password != "test"
    assert PasswordHasher().verify(user.password, "test")
