from typing import Any, Dict

from src.db import BlacklistedToken
from src.auth import get_token_expiry


async def test_blacklisted_token__str(access_token: str) -> None:
    token = await BlacklistedToken.create(
        token=access_token, expiry=get_token_expiry(access_token)
    )
    assert str(token) == f"<token {token.id}>"


async def test_blacklisted_token__dict(access_token: str) -> None:
    token = await BlacklistedToken.create(
        token=access_token, expiry=get_token_expiry(access_token)
    )
    token_: Dict[str, Any] = dict(token)
    for field in (
        "id",
        "token",
        "expiry",
        "created_on",
        "last_modified",
    ):
        assert token_[field] == getattr(token, field)
    assert len(token_) == 5
