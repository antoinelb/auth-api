from datetime import datetime, timedelta
from typing import Dict, List

import jwt
import pytest
from pytest_mock import MockerFixture  # type: ignore
from starlette.testclient import TestClient

from src import config
from src.auth import create_token, get_token_expiry, verify_token
from src.db import BlacklistedToken, User


@pytest.mark.parametrize("refresh", [True, False])
async def test_create_token(refresh: bool, user: User) -> None:
    token = create_token(user, refresh)
    payload = jwt.decode(
        token,
        str(config.TOKEN_KEY),
        audience=config.TOKEN_AUD,
        algorithms=["HS256"],
    )
    assert payload["aud"] == config.TOKEN_AUD
    assert datetime.fromtimestamp(payload["iat"]) < datetime.utcnow()
    assert datetime.fromtimestamp(payload["exp"]) < datetime.utcnow() + (
        timedelta(minutes=config.REFRESH_TOKEN_EXPIRY + 1)
        if refresh
        else timedelta(minutes=config.ACCESS_TOKEN_EXPIRY + 1)
    )
    assert payload["username"] == user.username

    if not refresh:
        assert "is_admin" in payload


@pytest.mark.parametrize("refresh", [True, False])
async def test_verify_token(
    user: User,
    access_token: str,
    refresh_token: str,
    refresh: bool,
) -> None:
    token = refresh_token if refresh else access_token
    user_ = await verify_token(token, refresh)
    assert user == user_


@pytest.mark.parametrize("refresh", [True, False])
async def test_verify_token__doesnt_exist(refresh: bool) -> None:
    token = jwt.encode(
        {
            "username": "test",
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() + timedelta(minutes=1),
        },
        str(config.TOKEN_KEY),
    )

    with pytest.raises(ValueError):
        await verify_token(token, refresh)  # type: ignore


@pytest.mark.parametrize("refresh", [True, False])
async def test_verify_token__wrong_payload(refresh: bool) -> None:
    token = jwt.encode(
        {
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() + timedelta(minutes=1),
        },
        str(config.TOKEN_KEY),
    )
    with pytest.raises(ValueError) as exp:
        await verify_token(token, refresh)  # type: ignore
    assert str(exp.value) == "The token is invalid."


@pytest.mark.parametrize("refresh", [True, False])
async def test_verify_token__wrong_format(refresh: bool) -> None:
    with pytest.raises(ValueError) as exp:
        await verify_token("wrong", refresh)
    assert str(exp.value) == "The token is invalid."


@pytest.mark.parametrize("refresh", [True, False])
async def test_verify_token__expired_token(refresh: bool) -> None:
    token = jwt.encode(
        {
            "username": "test",
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() - timedelta(minutes=1),
        },
        str(config.TOKEN_KEY),
    )
    with pytest.raises(ValueError) as exp:
        await verify_token(token, refresh)  # type: ignore
    assert str(exp.value) == "The token expired."


async def test_verify_token__blacklisted_token(refresh_token: str) -> None:
    await BlacklistedToken.create(
        token=refresh_token, expiry=datetime.utcnow()
    )
    with pytest.raises(ValueError) as exp:
        await verify_token(refresh_token, True)
    assert str(exp.value) == "The token was blacklisted."


async def test_get_token_expiry() -> None:
    expiry = datetime.utcnow() + timedelta(minutes=1)
    token = jwt.encode(
        {
            "username": "test",
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": expiry,
        },
        str(config.TOKEN_KEY),
    )
    assert get_token_expiry(token) == expiry.replace(  # type: ignore
        microsecond=0
    )


async def test_get_token_expiry__wrong_format() -> None:
    with pytest.raises(ValueError) as exp:
        get_token_expiry("wrong")
    assert str(exp.value) == "The token is invalid."


async def test_get_token_expiry__expired() -> None:
    expiry = datetime.utcnow() - timedelta(minutes=1)
    token = jwt.encode(
        {
            "username": "test",
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": expiry,
        },
        str(config.TOKEN_KEY),
    )
    with pytest.raises(ValueError) as exp:
        get_token_expiry(token)  # type: ignore
    assert str(exp.value) == "The token expired."


async def test_auth_middleware__not_secure(client: TestClient) -> None:
    resp = client.get("/")
    assert resp.status_code == 200
    assert resp.text == "Healthy!"


async def test_auth_middleware__secure__passes(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    resp = client.get("/authenticated", headers=auth_headers)
    assert resp.status_code == 200
    assert resp.text == "Authenticated!"


async def test_auth_middleware__secure__missing_refresh_token(
    client: TestClient,
) -> None:
    resp = client.get("/secure")
    assert resp.status_code == 401
    assert resp.text == "You don't have access to this resource."


async def test_auth_middleware__secure__invalid_refresh_token(
    mocker: MockerFixture, client: TestClient, auth_headers: Dict[str, str]
) -> None:
    mocker.patch("src.auth.verify_token", side_effect=ValueError())
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == "You don't have access to this resource."


async def test_auth_middleware__secure__expired_refresh_token(
    mocker: MockerFixture, client: TestClient, auth_headers: Dict[str, str]
) -> None:
    mocker.patch(
        "src.auth.verify_token", side_effect=ValueError("The token expired.")
    )
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == (
        "Your session has expired. "
        "You need to sign out and back in to do this."
    )


async def test_auth_middleware__secure__blacklisted_refresh_token(
    mocker: MockerFixture, client: TestClient, auth_headers: Dict[str, str]
) -> None:
    mocker.patch(
        "src.auth.verify_token",
        side_effect=ValueError("The token was blacklisted."),
    )
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == (
        "Your session has expired. "
        "You need to sign out and back in to do this."
    )


async def test_auth_middleware__secure__missing_access(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    del auth_headers["access-token"]
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == "The token expired."


async def test_auth_middleware__secure__invalid_access_token(
    mocker: MockerFixture,
    client: TestClient,
    auth_headers: Dict[str, str],
    user: User,
) -> None:
    mocker.patch("src.auth.verify_token", side_effect=[user, ValueError()])
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == "You don't have access to this resource."


async def test_auth_middleware__secure__expired_access_token(
    mocker: MockerFixture,
    client: TestClient,
    auth_headers: Dict[str, str],
    user: User,
) -> None:
    mocker.patch(
        "src.auth.verify_token",
        side_effect=[user, ValueError("The token expired.")],
    )
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == "The token expired."


async def test_auth_middleware__secure__access_and_refresh_dontcorrespond(
    mocker: MockerFixture,
    client: TestClient,
    auth_headers: Dict[str, str],
    user: User,
    users: List[User],
) -> None:
    mocker.patch("src.auth.verify_token", side_effect=[user, users[0]])
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == "You don't have access to this resource."


async def test_auth_middleware__secure__missing_access__signout(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    del auth_headers["access-token"]
    resp = client.post("/users/signout", headers=auth_headers)
    assert resp.status_code == 200
