from pathlib import Path
from unittest.mock import patch

from starlette.testclient import TestClient


async def test_health_check(client: TestClient) -> None:
    resp = client.get("/")
    assert resp.status_code == 200
    assert resp.text == "Healthy!"


async def test_version(client: TestClient, tmp_path: Path) -> None:
    with patch("src.api.api.pyproject_path", tmp_path / "pyproject.toml"):
        with open(tmp_path / "pyproject.toml", "w") as f:
            f.write("[tool.poetry]\n")
            f.write('version="0.0.1"\n')
        resp = client.get("/version")
        assert resp.status_code == 200
        assert resp.text == "0.0.1"

        with open(tmp_path / "pyproject.toml", "w") as f:
            f.write("[tool.poetry]\n")
            f.write('version="1.0.1"\n')
        resp = client.get("/version")
        assert resp.status_code == 200
        assert resp.text == "1.0.1"

        with open(tmp_path / "pyproject.toml", "w") as f:
            f.write("[tool.poetry]\n")
        resp = client.get("/version")
        assert resp.status_code == 500
        assert resp.text == "Unknown version"
