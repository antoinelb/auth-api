import json
from typing import Any, Dict, List

import pytest
from _pytest.logging import LogCaptureFixture
from starlette.testclient import TestClient

from src.db import User


async def test_get_users(
    client: TestClient,
    users: List[User],
    admin: User,
    admin_auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/admin/users/", headers=admin_auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert all(
        d["username"] == u.username and d["is_admin"] == u.is_admin
        for d, u in zip(data, [*users, admin])
    )


async def test_get_user(
    client: TestClient, user: User, admin_auth_headers: Dict[str, str]
) -> None:
    resp = client.get(
        f"/admin/users/get?username={user.username}",
        headers=admin_auth_headers,
    )
    assert resp.status_code == 200
    data = resp.json()
    assert data["username"] == user.username
    assert data["is_admin"] == user.is_admin


async def test_get_user__doesnt_exist(
    client: TestClient, admin_auth_headers: Dict[str, str]
) -> None:
    resp = client.get(
        "/admin/users/get?username=test", headers=admin_auth_headers
    )
    assert resp.status_code == 400
    assert resp.text == "The user test doesn't exist."


@pytest.mark.parametrize(
    "field,val",
    [("is_admin", True), ("pin", "0123")],
)
async def test_update_user(
    client: TestClient,
    admin_auth_headers: Dict[str, str],
    user: User,
    admin: User,
    caplog: LogCaptureFixture,
    field: str,
    val: Any,
) -> None:
    username = user.username

    setattr(user, field, val)

    resp = client.post(
        "/admin/users/update",
        data=json.dumps({"username": username, field: val}),
        headers=admin_auth_headers,
    )
    assert resp.status_code == 200
    user = await User.get(
        username=val if field == "new_username" else username
    )
    data = resp.json()
    if field == "new_username":
        assert data["username"] == val
        assert user.username == val

    assert (
        caplog.records[0].getMessage() == f"{admin} updated {user} "
        f"{'username' if field == 'new_username' else field}."
    )


async def test_update_user__doesnt_exist(
    client: TestClient,
    admin_auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/admin/users/update",
        data=json.dumps({"username": "test"}),
        headers=admin_auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The user test doesn't exist."


async def test_update_user__invalid_pin(
    client: TestClient,
    admin_auth_headers: Dict[str, str],
    user: User,
) -> None:
    username = user.username

    resp = client.post(
        "/admin/users/update",
        data=json.dumps({"username": username, "pin": "123"}),
        headers=admin_auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The pin is not in a valid format."

    resp = client.post(
        "/admin/users/update",
        data=json.dumps({"username": username, "pin": "a123"}),
        headers=admin_auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The pin is not in a valid format."


async def test_delete_user(
    client: TestClient,
    admin_auth_headers: Dict[str, str],
    user: User,
    admin: User,
    caplog: LogCaptureFixture,
) -> None:
    username = user.username

    resp = client.post(
        "/admin/users/delete",
        data=json.dumps({"username": username}),
        headers=admin_auth_headers,
    )
    assert resp.status_code == 200

    assert not await User.filter(username=username)

    assert caplog.records[0].getMessage() == f"{admin} deleted {user}."


async def test_delete_user__doesnt_exist(
    client: TestClient,
    admin_auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/admin/users/delete",
        data=json.dumps({"username": "test"}),
        headers=admin_auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The user test doesn't exist."
