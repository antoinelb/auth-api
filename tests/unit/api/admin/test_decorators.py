from unittest.mock import Mock

from src.api.admin.decorators import admin_required
from src.db import User


async def test_admin_required__admin(admin: User) -> None:
    async def test(req: Mock) -> None:  # pylint: disable=unused-argument
        return None

    req = Mock()
    req.scope = {"user": admin}
    fct = admin_required(test)  # type: ignore
    resp = await fct(req)
    assert resp is None


async def test_admin_required__regular(user: User) -> None:
    async def test(req: Mock) -> None:  # pylint: disable=unused-argument
        return None

    req = Mock()
    req.scope = {"user": user}
    fct = admin_required(test)  # type: ignore
    resp = await fct(req)
    assert resp.status_code == 401
    assert resp.body.decode() == "You don't have access to this resource."


async def test_admin_required__no_user() -> None:
    async def test(req: Mock) -> None:  # pylint: disable=unused-argument
        return None

    req = Mock()
    req.scope = {}
    fct = admin_required(test)  # type: ignore
    resp = await fct(req)
    assert resp.status_code == 401
    assert resp.body.decode() == "You don't have access to this resource."
