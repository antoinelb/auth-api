import json
from typing import Dict

import jwt
import pytest
from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from starlette.testclient import TestClient

from src import config
from src.db import BlacklistedToken, User


async def test_sign_up(client: TestClient) -> None:
    resp = client.post(
        "/users/signup",
        data=json.dumps({"username": "test", "password": "test"}),
    )
    assert resp.status_code == 200
    data = resp.json()
    jwt.decode(
        data["access-token"],
        key=str(config.TOKEN_KEY),
        audience=config.TOKEN_AUD,
        algorithms=["HS256"],
    )
    jwt.decode(
        data["refresh-token"],
        key=str(config.TOKEN_KEY),
        audience=config.TOKEN_AUD,
        algorithms=["HS256"],
    )
    user = await User.get(username="test")
    assert user.password != "test"


async def test_sign_up__already_exists(client: TestClient, user: User) -> None:
    resp = client.post(
        "/users/signup",
        data=json.dumps({"username": user.username, "password": "test"}),
    )
    assert resp.status_code == 400
    assert (
        resp.text == f"There already is a user with username {user.username}."
    )


async def test_sign_in(client: TestClient, user: User) -> None:
    resp = client.post(
        "/users/signin",
        data=json.dumps({"username": user.username, "password": "test"}),
    )
    assert resp.status_code == 200
    data = resp.json()
    jwt.decode(
        data["access-token"],
        key=str(config.TOKEN_KEY),
        audience=config.TOKEN_AUD,
        algorithms=["HS256"],
    )
    jwt.decode(
        data["refresh-token"],
        key=str(config.TOKEN_KEY),
        audience=config.TOKEN_AUD,
        algorithms=["HS256"],
    )


async def test_sign_in__unknown_user(client: TestClient) -> None:
    resp = client.post(
        "/users/signin",
        data=json.dumps({"username": "test", "password": "test"}),
    )
    assert resp.status_code == 400
    assert resp.text == "Either the username or password are wrong."


async def test_sign_in__wrong_password(client: TestClient, user: User) -> None:
    resp = client.post(
        "/users/signin",
        data=json.dumps({"username": user.username, "password": "wrong"}),
    )
    assert resp.status_code == 400
    assert resp.text == "Either the username or password are wrong."


async def test_sign_out(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    resp = client.post("/users/signout", headers=auth_headers)
    assert resp.status_code == 200
    assert await BlacklistedToken.filter(
        token=auth_headers["refresh-token"]
    ).exists()


async def test_refresh(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    resp = client.get("/users/refresh", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    access_token = data["access-token"]
    jwt.decode(
        access_token,
        key=str(config.TOKEN_KEY),
        audience=config.TOKEN_AUD,
        algorithms=["HS256"],
    )

    resp = client.get(
        "/users/refresh",
        headers={"refresh-token": auth_headers["refresh-token"]},
    )
    assert resp.status_code == 200
    data = resp.json()
    access_token = data["access-token"]
    jwt.decode(
        access_token,
        key=str(config.TOKEN_KEY),
        audience=config.TOKEN_AUD,
        algorithms=["HS256"],
    )


async def test_info__regular(
    client: TestClient,
    user: User,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/users/info", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert data["username"] == user.username
    assert not data["is_admin"]


async def test_info__admin(
    client: TestClient,
    admin: User,
    admin_auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/users/info", headers=admin_auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert data["username"] == admin.username
    assert data["is_admin"]


async def test_update_pin(
    client: TestClient, auth_headers: Dict[str, str], user: User
) -> None:
    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "0123", "previous_pin": None}),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    assert (await User.get(username=user.username)).pin == "0123"

    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "1234", "previous_pin": "0123"}),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    assert (await User.get(username=user.username)).pin == "1234"

    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": None, "previous_pin": "1234"}),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    assert (await User.get(username=user.username)).pin is None


async def test_update_pin__wrong_pin_format(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "123", "previous_pin": None}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The pin must be a number with 4 digits."

    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "a123", "previous_pin": None}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The pin must be a number with 4 digits."

    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "0123", "previous_pin": "123"}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The previous pin must be a number with 4 digits."

    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "0123", "previous_pin": "a123"}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The previous pin must be a number with 4 digits."


async def test_update_pin__wrong_pin(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "0123", "previous_pin": "0123"}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The previous pin is wrong."

    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "0123", "previous_pin": None}),
        headers=auth_headers,
    )
    assert resp.status_code == 200

    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "0123", "previous_pin": "1234"}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The previous pin is wrong."


async def test_update_password(
    client: TestClient, auth_headers: Dict[str, str], user: User
) -> None:
    resp = client.post(
        "/users/update-password",
        data=json.dumps({"password": "new", "previous_password": "test"}),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    user = await User.get(username=user.username)
    with pytest.raises(VerifyMismatchError):
        PasswordHasher().verify(user.password, "test")
    PasswordHasher().verify(user.password, "new")

    assert await BlacklistedToken.filter(
        token=auth_headers["refresh-token"]
    ).exists()


async def test_update_password__wrong_password(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    resp = client.post(
        "/users/update-password",
        data=json.dumps({"password": "new", "previous_password": "wrong"}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The password is wrong."
