from unittest.mock import AsyncMock

import pytest
from pytest_mock import MockerFixture  # type: ignore
from tortoise.exceptions import DoesNotExist
from typer import Typer
from typer.testing import CliRunner

from src.cli import init_cli, run_cli


def test_cli(runner: CliRunner, cli: Typer) -> None:
    resp = runner.invoke(cli)
    assert resp.exit_code == 0
    assert resp.output.startswith("Usage")

    resp = runner.invoke(cli, ["--help"])
    assert resp.exit_code == 0
    assert resp.output.startswith("Usage")


def test_init_cli() -> None:
    cli = init_cli()
    assert isinstance(cli, Typer)


def test_run_cli(mocker: MockerFixture) -> None:
    init_cli = mocker.patch("src.cli.init_cli")
    run_cli()
    init_cli.assert_called_once()


@pytest.mark.parametrize("command", ["run", "r"])
def test_run(
    mocker: MockerFixture,
    runner: CliRunner,
    cli: Typer,
    command: str,
) -> None:
    run_server = mocker.patch("src.cli.app.run_server")

    resp = runner.invoke(cli, [command])
    assert resp.exit_code == 0
    run_server.assert_called_once()


@pytest.mark.parametrize("exists", [False, True])
def test_create_admin(
    mocker: MockerFixture,
    runner: CliRunner,
    cli: Typer,
    exists: bool,
) -> None:
    update_admin_status = mocker.patch(
        "src.cli.admin.update_admin_status",
        side_effect=AsyncMock(return_value=exists),
    )
    mocker.patch("src.cli.getpass", return_value="test")

    resp = runner.invoke(cli, ["create-admin", "test"])
    assert resp.exit_code == 0
    update_admin_status.assert_called_once()

    if exists:
        assert resp.output == "Admin status granted to test.\n"
    else:
        assert resp.output == "Admin user test created.\n"


def test_create_admin__passwords_dont_match(
    mocker: MockerFixture,
    runner: CliRunner,
    cli: Typer,
) -> None:
    mocker.patch("src.cli.getpass", side_effect=["test", "test2"])

    resp = runner.invoke(cli, ["create-admin", "test"])
    assert resp.exit_code == 1

    assert resp.output == "The passwords don't match.\n"


def test_create_admin__error_creating_user(
    mocker: MockerFixture,
    runner: CliRunner,
    cli: Typer,
) -> None:
    mocker.patch(
        "src.cli.admin.update_admin_status",
        side_effect=AsyncMock(return_value=False),
    )
    mocker.patch(
        "src.cli.admin.create_admin_user",
        side_effect=AsyncMock(return_value=False),
    )
    mocker.patch("src.cli.getpass", return_value="test")

    resp = runner.invoke(cli, ["create-admin", "test"])
    assert resp.exit_code == 1

    assert resp.output == "There was an error creating admin user test.\n"


@pytest.mark.parametrize("exists", [False, True])
def test_revoke_admin(
    mocker: MockerFixture,
    runner: CliRunner,
    cli: Typer,
    exists: bool,
) -> None:
    update_admin_status = mocker.patch(
        "src.cli.admin.update_admin_status",
        side_effect=AsyncMock(return_value=exists),
    )

    resp = runner.invoke(cli, ["remove-admin", "test"])
    assert resp.exit_code == 0
    update_admin_status.assert_called_once()

    if exists:
        assert resp.output == "Admin status revoked from test.\n"
    else:
        assert resp.output == "There is no user test.\n"


def test_set_password(
    mocker: MockerFixture, runner: CliRunner, cli: Typer
) -> None:
    set_password = mocker.patch("src.cli.admin.set_password")
    mocker.patch("src.cli.getpass", return_value="new")
    resp = runner.invoke(cli, ["set-password", "test"])
    assert resp.exit_code == 0
    assert resp.output == "Password updated.\n"
    set_password.assert_called_once()


def test_set_password__doesnt_exist(
    mocker: MockerFixture, runner: CliRunner, cli: Typer
) -> None:
    set_password = mocker.patch(
        "src.cli.admin.set_password", side_effect=DoesNotExist
    )
    mocker.patch("src.cli.getpass", return_value="new")
    resp = runner.invoke(cli, ["set-password", "test"])
    assert resp.exit_code == 1
    assert resp.output == "The user test doesn't exist.\n"
    set_password.assert_called_once()


def test_set_password__password_mismatch(
    mocker: MockerFixture, runner: CliRunner, cli: Typer
) -> None:
    set_password = mocker.patch("src.cli.admin.set_password")
    mocker.patch("src.cli.getpass", side_effect=["new", "new_"])
    resp = runner.invoke(cli, ["set-password", "test"])
    assert resp.exit_code == 1
    assert resp.output == "The passwords don't match.\n"
    set_password.assert_not_called()
