import logging
import os

from _pytest.logging import LogCaptureFixture
from pytest_mock import MockerFixture

from src.logging import init_logging, logger


def test_init_logging__dir_creation(mocker: MockerFixture) -> None:
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, os.pardir, "logs"
    )
    mocker.patch("src.logging.os.path.exists", return_value=False)
    mkdir = mocker.patch("src.logging.os.mkdir")
    init_logging()
    assert mkdir.calledOnceWith(path)


def test_ping_filter(caplog: LogCaptureFixture) -> None:
    init_logging()
    with caplog.at_level(logging.INFO, logger="auth"):
        logger.info('"GET /test HTTP/1.1" 200')
        logger.info('"GET /ping HTTP/1.1" 200')
    assert len([rec for rec in caplog.records if rec.name == "auth"]) == 2
