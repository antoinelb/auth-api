from typing import Optional

import pytest
from _pytest.logging import LogCaptureFixture
from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from tortoise.exceptions import DoesNotExist

from src.admin import create_admin_user, set_password, update_admin_status
from src.db import User


@pytest.mark.parametrize("user_", [None, "user", "admin"])
@pytest.mark.parametrize("is_admin", [True, False])
async def test_update_admin_status(
    caplog: LogCaptureFixture,
    user: User,
    admin: User,
    user_: Optional[str],
    is_admin: bool,
) -> None:
    if user_ is None:
        username = "new"
    elif user_ == "user":
        username = user.username
    elif user_ == "admin":
        username = admin.username

    resp = await update_admin_status(username, is_admin)  # type: ignore

    if user_ is None:
        assert not resp
        assert not caplog.records
    elif user_ == "user" and not is_admin:
        assert resp
        assert not caplog.records
        assert not (await User.get(username=user.username)).is_admin
    elif user_ == "admin" and is_admin:
        assert resp
        assert not caplog.records
        assert (await User.get(username=admin.username)).is_admin
    elif user_ == "user":
        assert resp
        assert (
            caplog.records[0].getMessage()
            == f"<admin user {user.username}> was granted admin status."
        )
        assert (await User.get(username=user.username)).is_admin
    elif user_ == "admin":
        assert resp
        assert (
            caplog.records[0].getMessage()
            == f"<user {admin.username}> was revoked admin status."
        )
        assert not (await User.get(username=admin.username)).is_admin


async def test_create_admin_user(
    caplog: LogCaptureFixture,
) -> None:
    assert await create_admin_user("test", "test")
    user = await User.get(username="test")
    PasswordHasher().verify(user.password, "test")
    assert not (await create_admin_user("test", "test"))


async def test_set_password(caplog: LogCaptureFixture, user: User) -> None:
    PasswordHasher().verify(user.password, "test")
    await set_password(user.username, "new")
    await user.refresh_from_db()
    PasswordHasher().verify(user.password, "new")
    with pytest.raises(VerifyMismatchError):
        PasswordHasher().verify(user.password, "test")
    assert (
        caplog.records[0].getMessage()
        == f"Updated password for <user {user.username}>."
    )


async def test_set_password__doesnt_exist(caplog: LogCaptureFixture) -> None:
    with pytest.raises(DoesNotExist):
        await set_password("test", "new")
    assert not caplog.records
