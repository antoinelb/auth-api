import json

from starlette.testclient import TestClient

from src.db import User


async def test_update_pin(
    client: TestClient, user: User, auth_headers: dict[str, str]
) -> None:
    # Get info
    resp = client.get("/users/info", headers=auth_headers)
    assert resp.status_code == 200
    info = resp.json()

    # Create new pin
    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "1234", "previous_pin": None}),
        headers=auth_headers,
    )
    assert resp.status_code == 200

    resp = client.get("/users/info", headers=auth_headers)
    assert resp.status_code == 200
    info = resp.json()
    assert info["pin"] == "1234"

    # Update pin
    resp = client.post(
        "/users/update-pin",
        data=json.dumps({"pin": "5678", "previous_pin": "1234"}),
        headers=auth_headers,
    )
    assert resp.status_code == 200

    resp = client.get("/users/info", headers=auth_headers)
    assert resp.status_code == 200
    info = resp.json()
    assert info["pin"] == "5678"
