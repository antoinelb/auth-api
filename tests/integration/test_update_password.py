import json

import pytest
from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from starlette.testclient import TestClient

from src.db import BlacklistedToken, User


async def test_update_password(
    client: TestClient, user: User, auth_headers: dict[str, str]
) -> None:
    resp = client.post(
        "/users/update-password",
        data=json.dumps({"password": "new", "previous_password": "test"}),
        headers=auth_headers,
    )
    assert resp.status_code == 200

    user = await User.get(username=user.username)
    with pytest.raises(VerifyMismatchError):
        PasswordHasher().verify(user.password, "test")
    PasswordHasher().verify(user.password, "new")

    assert await BlacklistedToken.filter(
        token=auth_headers["refresh-token"]
    ).exists()
