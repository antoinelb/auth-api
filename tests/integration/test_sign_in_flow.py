import json

from starlette.testclient import TestClient

from src.db import User


async def test_sign_in_flow__new_user(client: TestClient) -> None:
    user = "test"
    password = "pass"

    # sign up
    resp = client.post(
        "/users/signup",
        data=json.dumps({"username": user, "password": password}),
    )
    assert resp.status_code == 200
    data = resp.json()
    tokens = {
        "access-token": data["access-token"],
        "refresh-token": data["refresh-token"],
    }

    # Get info
    resp = client.get("/users/info", headers=tokens)
    assert resp.status_code == 200
    data = resp.json()
    assert data["username"] == user

    # Get new access token
    resp = client.get(
        "/users/refresh", headers={"refresh-token": tokens["refresh-token"]}
    )
    assert resp.status_code == 200
    assert "access-token" in resp.json()

    # Logout
    resp = client.post("/users/signout", headers=tokens)
    assert resp.status_code == 200
    resp = client.get("/users/info")
    assert resp.status_code == 401


async def test_sign_in_flow__existing_user(
    client: TestClient, user: User
) -> None:

    # sign in
    resp = client.post(
        "/users/signin",
        data=json.dumps({"username": user.username, "password": "test"}),
    )
    assert resp.status_code == 200
    data = resp.json()
    tokens = {
        "access-token": data["access-token"],
        "refresh-token": data["refresh-token"],
    }

    # Get info
    resp = client.get("/users/info", headers=tokens)
    assert resp.status_code == 200
    data = resp.json()
    assert data["username"] == user.username

    # Get new access token
    resp = client.get(
        "/users/refresh", headers={"refresh-token": tokens["refresh-token"]}
    )
    assert resp.status_code == 200
    assert "access-token" in resp.json()

    # Logout
    resp = client.post("/users/signout", headers=tokens)
    assert resp.status_code == 200
    resp = client.get("/users/info")
    assert resp.status_code == 401
