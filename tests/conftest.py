from typing import List

import nest_asyncio
from _pytest.python import Function

from .fixtures import *  # noqa

nest_asyncio.apply()


def pytest_collection_modifyitems(items: List[Function]) -> None:
    for item in items:
        item.add_marker("asyncio")
