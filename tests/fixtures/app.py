import pytest
from src.app import create_app
from starlette.applications import Starlette
from starlette.testclient import TestClient


@pytest.fixture
def app() -> Starlette:
    return create_app()


@pytest.fixture
def client(app: Starlette) -> TestClient:
    return TestClient(app)
