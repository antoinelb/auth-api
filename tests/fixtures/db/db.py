import os
from typing import AsyncIterator

import pytest
from src.db import close_db, empty_db, init_db


@pytest.fixture(scope="session")
def init_db_() -> None:
    os.chdir(
        os.path.join(
            os.path.dirname(__file__), os.pardir, os.pardir, os.pardir
        )
    )
    os.system("aerich upgrade")  # nosec


@pytest.fixture(autouse=True)
async def db(
    init_db_: None,  # pylint: disable=unused-argument
) -> AsyncIterator[None]:
    await init_db()
    await empty_db()
    yield
    await init_db()
    await empty_db()
    await close_db()
