from typing import List

import pytest

from src.db import User

from .db import *  # noqa


@pytest.fixture
async def user() -> User:
    return await User.create(username="test", password="test")


@pytest.fixture
async def users() -> List[User]:
    return [
        await User.create(username=f"test_{i}", password="test")
        for i in range(3)
    ]


@pytest.fixture
async def admin() -> User:
    return await User.create(username="admin", password="test", is_admin=True)
