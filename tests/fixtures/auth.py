from datetime import datetime, timedelta
from typing import Dict

import jwt
import pytest

from src import config
from src.db import User

from .db import *  # noqa


@pytest.fixture
def refresh_token(user: User) -> str:
    payload = {
        "username": user.username,
        "aud": config.TOKEN_AUD,
        "iat": datetime.utcnow(),
        "exp": datetime.utcnow() + timedelta(minutes=1),
    }
    return jwt.encode(payload, str(config.TOKEN_KEY))


@pytest.fixture
def access_token(user: User) -> str:
    payload = {
        "username": user.username,
        "is_admin": False,
        "aud": config.TOKEN_AUD,
        "iat": datetime.utcnow(),
        "exp": datetime.utcnow() + timedelta(minutes=1),
    }
    return jwt.encode(payload, str(config.TOKEN_KEY))


@pytest.fixture
def auth_headers(access_token: str, refresh_token: str) -> Dict[str, str]:
    return {"access-token": access_token, "refresh-token": refresh_token}


@pytest.fixture
def admin_auth_headers(admin: User) -> Dict[str, str]:
    access_token = jwt.encode(
        {
            "username": admin.username,
            "is_admin": True,
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() + timedelta(minutes=1),
        },
        str(config.TOKEN_KEY),
    )
    refresh_token = jwt.encode(
        {
            "username": admin.username,
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() + timedelta(minutes=1),
        },
        str(config.TOKEN_KEY),
    )
    return {"access-token": access_token, "refresh-token": refresh_token}
