import logging
from pathlib import Path
from unittest.mock import patch

import pytest
from _pytest.logging import LogCaptureFixture


@pytest.fixture(autouse=True)
def log_dir(tmp_path: Path) -> Path:
    path = tmp_path / "logs"
    patch("src.logging.log_dir", path)
    return path


@pytest.fixture(autouse=True)
def init_logging(caplog: LogCaptureFixture, log_dir: Path) -> None:
    caplog.set_level(logging.ERROR)
    caplog.set_level(logging.INFO, "auth")
