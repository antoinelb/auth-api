-- upgrade --
ALTER TABLE "users" ALTER COLUMN "pin" TYPE VARCHAR(4) USING "pin"::VARCHAR(4);
-- downgrade --
ALTER TABLE "users" ALTER COLUMN "pin" TYPE VARCHAR(16) USING "pin"::VARCHAR(16);
