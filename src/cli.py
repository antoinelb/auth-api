import asyncio
import sys
from getpass import getpass

import typer
from tortoise.exceptions import DoesNotExist

from . import admin, app
from .db import close_db, init_db
from .logging import init_logging


def init_cli() -> typer.Typer:
    cli = typer.Typer()
    cli.command("run")(run)
    cli.command("r", hidden=True)(run)
    cli.command("create-admin")(create_admin)
    cli.command("remove-admin")(remove_admin)
    cli.command("set-password")(set_password)
    return cli


def run_cli() -> None:
    cli = init_cli()
    cli()


def run() -> None:
    app.run_server()


def create_admin(username: str) -> None:
    init_logging()
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(init_db())
        updated = loop.run_until_complete(
            admin.update_admin_status(username, is_admin=True)
        )
        if updated:
            typer.echo(f"Admin status granted to {username}.")
        else:
            password = getpass("Password: ")
            password_ = getpass("Confirm password: ")
            if password != password_:
                typer.echo("The passwords don't match.")
                sys.exit(1)
            created = loop.run_until_complete(
                admin.create_admin_user(username, password)
            )
            if created:
                typer.echo(f"Admin user {username} created.")
            else:
                typer.echo(
                    f"There was an error creating admin user {username}."
                )
                sys.exit(1)
    finally:
        loop.run_until_complete(close_db())


def remove_admin(username: str) -> None:
    init_logging()
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(init_db())
        updated = loop.run_until_complete(
            admin.update_admin_status(username, is_admin=False)
        )
        if updated:
            typer.echo(f"Admin status revoked from {username}.")
        else:
            typer.echo(f"There is no user {username}.")
    finally:
        loop.run_until_complete(close_db())


def set_password(username: str) -> None:
    init_logging()
    password = getpass("Password: ")
    password_ = getpass("Confirm password: ")
    if password != password_:
        typer.echo("The passwords don't match.")
        sys.exit(1)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(init_db())
        loop.run_until_complete(admin.set_password(username, password))
        typer.echo("Password updated.")
    except DoesNotExist:
        typer.echo(f"The user {username} doesn't exist.")
        sys.exit(1)
    finally:
        loop.run_until_complete(close_db())
