from datetime import datetime, timedelta
from typing import Awaitable, Callable

import jwt
import jwt.exceptions
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from tortoise.exceptions import DoesNotExist

from . import config
from .db import BlacklistedToken, User


def create_token(user: User, refresh: bool) -> str:
    expiry = (
        config.REFRESH_TOKEN_EXPIRY if refresh else config.ACCESS_TOKEN_EXPIRY
    )

    payload = {
        "username": user.username,
        "aud": config.TOKEN_AUD,
        "iat": datetime.utcnow(),
        "exp": datetime.utcnow() + timedelta(minutes=expiry),
    }
    if not refresh:
        payload["is_admin"] = user.is_admin
    key = str(config.TOKEN_KEY)
    return jwt.encode(payload, key)  # type: ignore


async def verify_token(token: str, refresh: bool) -> User:
    key = str(config.TOKEN_KEY)
    try:
        payload = jwt.decode(
            token, key, audience=config.TOKEN_AUD, algorithms=["HS256"]
        )
    except jwt.exceptions.ExpiredSignatureError as exc:
        raise ValueError("The token expired.") from exc
    except Exception as exc:
        raise ValueError("The token is invalid.") from exc

    if refresh and await BlacklistedToken.filter(token=token).exists():
        raise ValueError("The token was blacklisted.")

    try:
        user = await User.get(username=payload["username"])
    except DoesNotExist as exc:
        raise ValueError("The user doesn't exist.") from exc
    except KeyError as exc:
        raise ValueError("The token is invalid.") from exc

    return user


def get_token_expiry(token: str) -> datetime:
    key = str(config.TOKEN_KEY)
    try:
        payload = jwt.decode(
            token, key, audience=config.TOKEN_AUD, algorithms=["HS256"]
        )
    except jwt.exceptions.ExpiredSignatureError as exc:
        raise ValueError("The token expired.") from exc
    except Exception as exc:
        raise ValueError("The token is invalid.") from exc

    return datetime.utcfromtimestamp(payload["exp"])


class AuthMiddleware(BaseHTTPMiddleware):
    async def dispatch(
        self: "AuthMiddleware",
        req: Request,
        call_next: Callable[[Request], Awaitable[Response]],
    ) -> Response:
        whitelisted = [
            f"/{path}" if not path.startswith("/") else path
            for path in config.WHITELISTED_URIS
        ]
        if req["path"] in whitelisted or req.method == "OPTIONS":
            return await call_next(req)

        access_token = req.headers.get("access-token")
        refresh_token = req.headers.get("refresh-token")

        if refresh_token is None:
            return PlainTextResponse(
                "You don't have access to this resource.", status_code=401
            )

        try:
            user_ = await verify_token(refresh_token, refresh=True)
        except ValueError as exc:
            if (
                str(exc) == "The token expired."
                or str(exc) == "The token was blacklisted."
            ):
                return PlainTextResponse(
                    "Your session has expired. "
                    "You need to sign out and back in to do this.",
                    status_code=401,
                )
            else:
                return PlainTextResponse(
                    "You don't have access to this resource.", status_code=401
                )

        if req["path"].endswith("signout") or req["path"].endswith("refresh"):
            req.scope["user"] = user_
            return await call_next(req)

        if access_token is None:
            return PlainTextResponse("The token expired.", status_code=401)

        try:
            user = await verify_token(access_token, refresh=False)
        except ValueError as exc:
            if str(exc) == "The token expired.":
                return PlainTextResponse("The token expired.", status_code=401)
            else:
                return PlainTextResponse(
                    "You don't have access to this resource.", status_code=401
                )

        if user != user_:
            return PlainTextResponse(
                "You don't have access to this resource.", status_code=401
            )

        req.scope["user"] = user
        return await call_next(req)
