import json
from datetime import datetime, timezone
from typing import Any, Awaitable, Callable, Dict, List, Optional, Union

from starlette.requests import Request
from starlette.responses import JSONResponse as _JSONResponse
from starlette.responses import PlainTextResponse, Response


async def get_json_params(
    req: Request,
    args: Optional[List[str]] = None,
    opt_args: Optional[List[str]] = None,
) -> Union[Dict[str, Any], Response]:
    if args is None:
        args = []
    if opt_args is None:
        opt_args = []
    try:
        data = json.loads(await req.body())
    except ValueError:
        return PlainTextResponse("Wrong data type was sent.", status_code=400)
    try:
        args_ = {arg: data[arg] for arg in args}
        opt_args_ = {arg: data[arg] for arg in opt_args if arg in data}
    except KeyError:
        return PlainTextResponse(
            "There are missing parameters.", status_code=400
        )
    return {**args_, **opt_args_}


async def get_query_string_params(
    req: Request,
    args: Optional[List[str]] = None,
    opt_args: Optional[List[str]] = None,
) -> Union[Dict[str, Any], Response]:
    if args is None:
        args = []
    if opt_args is None:
        opt_args = []
    try:
        args_ = {arg: req.query_params[arg] for arg in args}
        opt_args_ = {
            arg: req.query_params[arg]
            for arg in opt_args
            if arg in req.query_params
        }
    except KeyError:
        return PlainTextResponse(
            "There are missing parameters.", status_code=400
        )
    return {**args_, **opt_args_}


async def get_path_params(
    req: Request,
    args: Optional[List[str]] = None,
    opt_args: Optional[List[str]] = None,
) -> Union[Dict[str, Any], Response]:
    if args is None:
        args = []
    if opt_args is None:
        opt_args = []
    try:
        args_ = {arg: req.path_params[arg] for arg in args}
        opt_args_ = {
            arg: req.path_params[arg]
            for arg in opt_args
            if arg in req.path_params
        }
    except KeyError:
        return PlainTextResponse(
            "There are missing parameters.", status_code=400
        )
    return {**args_, **opt_args_}


async def get_headers(
    req: Request,
    args: Optional[List[str]] = None,
    opt_args: Optional[List[str]] = None,
) -> Union[Dict[str, Any], Response]:
    if args is None:
        args = []
    if opt_args is None:
        opt_args = []
    try:
        args_ = {arg: req.headers[arg] for arg in args}
        opt_args_ = {
            arg: req.headers[arg] for arg in opt_args if arg in req.headers
        }
    except KeyError:
        return PlainTextResponse("There are missing headers.", status_code=400)
    return {**args_, **opt_args_}


def with_json_params(
    args: Optional[List[str]] = None, opt_args: Optional[List[str]] = None
) -> Callable[[Callable], Callable]:
    def decorator(
        fct: Callable[..., Awaitable[Response]]
    ) -> Callable[..., Awaitable[Response]]:
        async def wrapper(
            req: Request, *args_: Any, **kwargs_: Any
        ) -> Response:
            params = await get_json_params(req, args=args, opt_args=opt_args)
            if isinstance(params, Response):
                return params
            params = {
                arg.replace("-", "_"): val for arg, val in params.items()
            }
            return await fct(req, *args_, **kwargs_, **params)

        return wrapper

    return decorator


def with_query_string_params(
    args: Optional[List[str]] = None, opt_args: Optional[List[str]] = None
) -> Callable[[Callable], Callable]:
    def decorator(
        fct: Callable[..., Awaitable[Response]]
    ) -> Callable[..., Awaitable[Response]]:
        async def wrapper(
            req: Request, *args_: Any, **kwargs_: Any
        ) -> Response:
            params = await get_query_string_params(
                req, args=args, opt_args=opt_args
            )
            if isinstance(params, Response):
                return params
            params = {
                arg.replace("-", "_"): val for arg, val in params.items()
            }
            return await fct(req, *args_, **kwargs_, **params)

        return wrapper

    return decorator


def with_path_params(
    args: Optional[List[str]] = None, opt_args: Optional[List[str]] = None
) -> Callable[[Callable], Callable]:
    def decorator(
        fct: Callable[..., Awaitable[Response]]
    ) -> Callable[..., Awaitable[Response]]:
        async def wrapper(
            req: Request, *args_: Any, **kwargs_: Any
        ) -> Response:
            params = await get_path_params(req, args=args, opt_args=opt_args)
            if isinstance(params, Response):
                return params
            params = {
                arg.replace("-", "_"): val for arg, val in params.items()
            }
            return await fct(req, *args_, **kwargs_, **params)

        return wrapper

    return decorator


def with_headers(
    args: Optional[List[str]] = None, opt_args: Optional[List[str]] = None
) -> Callable[[Callable], Callable]:
    def decorator(
        fct: Callable[..., Awaitable[Response]]
    ) -> Callable[..., Awaitable[Response]]:
        async def wrapper(
            req: Request, *args_: Any, **kwargs_: Any
        ) -> Response:
            params = await get_headers(req, args=args, opt_args=opt_args)
            if isinstance(params, Response):
                return params
            params = {
                arg.replace("-", "_"): val for arg, val in params.items()
            }
            return await fct(req, *args_, **kwargs_, **params)

        return wrapper

    return decorator


def JSONResponse(data: Any, *args: Any, **kwargs: Any) -> _JSONResponse:
    return _JSONResponse(convert_datetimes_for_json(data), *args, **kwargs)


def convert_datetimes_for_json(data: Any) -> Any:
    if isinstance(data, dict):
        return {
            key: convert_datetimes_for_json(val) for key, val in data.items()
        }
    elif isinstance(data, list):
        return [convert_datetimes_for_json(val) for val in data]
    elif isinstance(data, datetime):
        return int(data.replace(tzinfo=timezone.utc).timestamp())
    else:
        return data
