from typing import List, Optional, cast

from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.routing import Route
from tortoise.exceptions import DoesNotExist, IntegrityError

from ..auth import create_token, get_token_expiry
from ..db import BlacklistedToken, User
from .utils import JSONResponse, with_json_params


def get_routes() -> List[Route]:
    return [
        Route("/info", get_info, methods=["GET"]),
        Route("/refresh", refresh, methods=["GET"]),
        Route("/signin", sign_in, methods=["POST"]),
        Route("/signout", sign_out, methods=["POST"]),
        Route("/signup", sign_up, methods=["POST"]),
        Route("/update-password", update_password, methods=["POST"]),
        Route("/update-pin", update_pin, methods=["POST"]),
    ]


@with_json_params(args=["username", "password"])
async def sign_up(req: Request, username: str, password: str) -> Response:
    try:
        user = await User.create(username=username, password=password)
    except IntegrityError:
        return PlainTextResponse(
            f"There already is a user with username {username}.", 400
        )

    return JSONResponse(
        {
            "access-token": create_token(user, refresh=False),
            "refresh-token": create_token(user, refresh=True),
        }
    )


@with_json_params(args=["username", "password"])
async def sign_in(req: Request, username: str, password: str) -> Response:
    try:
        user = await User.get(username=username)
    except DoesNotExist:
        return PlainTextResponse(
            "Either the username or password are wrong.", 400
        )

    try:
        PasswordHasher().verify(user.password, password)
    except VerifyMismatchError:
        return PlainTextResponse(
            "Either the username or password are wrong.", 400
        )

    return JSONResponse(
        {
            "access-token": create_token(user, refresh=False),
            "refresh-token": create_token(user, refresh=True),
        }
    )


async def sign_out(req: Request) -> Response:
    token = cast(str, req.headers.get("refresh-token"))
    await BlacklistedToken.create(token=token, expiry=get_token_expiry(token))
    return PlainTextResponse("")


async def refresh(req: Request) -> Response:
    return JSONResponse(
        {"access-token": create_token(req.scope["user"], refresh=False)}
    )


async def get_info(req: Request) -> Response:
    return JSONResponse(req.scope["user"].info)


@with_json_params(args=["pin", "previous_pin"])
async def update_pin(
    req: Request, pin: Optional[str], previous_pin: Optional[str]
) -> Response:
    if pin is not None and (len(pin) != 4 or not pin.isdigit()):
        return PlainTextResponse(
            "The pin must be a number with 4 digits.", 400
        )
    if previous_pin is not None and (
        len(previous_pin) != 4 or not previous_pin.isdigit()
    ):
        return PlainTextResponse(
            "The previous pin must be a number with 4 digits.",
            400,
        )

    user = await User.get(username=req.scope["user"].username)

    if (user.pin is None and previous_pin is not None) or (
        user.pin != previous_pin
    ):
        return PlainTextResponse("The previous pin is wrong.", 400)

    await user.update_from_dict({"pin": pin})
    await user.save()
    return PlainTextResponse(pin)


@with_json_params(args=["password", "previous_password"])
async def update_password(
    req: Request, password: str, previous_password: str
) -> Response:
    user = await User.get(username=req.scope["user"].username)

    try:
        PasswordHasher().verify(user.password, previous_password)
    except VerifyMismatchError:
        return PlainTextResponse("The password is wrong.", 400)

    await user.update_from_dict({"password": password})
    await user.save()

    token = cast(str, req.headers.get("refresh-token"))
    await BlacklistedToken.create(token=token, expiry=get_token_expiry(token))

    return PlainTextResponse("")
