from typing import List

from starlette.routing import BaseRoute, Mount

from . import users


def get_routes() -> List[BaseRoute]:
    return [
        Mount("/users", routes=users.get_routes()),
    ]
