from typing import Any, Dict, List, Optional

from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.routing import Route
from tortoise.exceptions import DoesNotExist

from src.db import User
from src.logging import logger
from src.utils import format_list

from ..utils import JSONResponse, with_json_params, with_query_string_params
from .decorators import admin_required


def get_routes() -> List[Route]:
    return [
        Route(
            "/",
            get_users,
            methods=["GET"],
            name="list",
        ),
        Route(
            "/get",
            get_user,
            methods=["GET"],
            name="get",
        ),
        Route(
            "/update",
            update_user,
            methods=["POST"],
            name="update",
        ),
        Route(
            "/delete",
            delete_user,
            methods=["POST"],
            name="delete",
        ),
    ]


@admin_required
async def get_users(_: Request) -> Response:
    return JSONResponse([user.info for user in await User.all()])


@admin_required
@with_query_string_params(args=["username"])
async def get_user(_: Request, username: str) -> Response:
    try:
        user = await User.get(username=username)
    except DoesNotExist:
        return PlainTextResponse(
            f"The user {username} doesn't exist.", status_code=400
        )
    return JSONResponse(user.info)


@admin_required
@with_json_params(opt_args=["username", "new_username", "is_admin", "pin"])
async def update_user(
    req: Request,
    username: str,
    is_admin: Optional[bool] = None,
    pin: Optional[str] = None,
) -> Response:
    try:
        user = await User.get(username=username)
    except DoesNotExist:
        return PlainTextResponse(
            f"The user {username} doesn't exist.", status_code=400
        )

    if pin is not None:
        if len(pin) != 4 or not pin.isdigit():
            return PlainTextResponse("The pin is not in a valid format.", 400)

    update: Dict[str, Any] = {
        **({"is_admin": is_admin} if is_admin is not None else {}),
        **({"pin": pin} if pin is not None else {}),
    }

    user = await user.update_from_dict(update)
    await user.save(force_update=True)

    logger.info(
        f"{req.scope['user']} updated {user} "
        f"{format_list(list(update.keys()))}."
    )

    return JSONResponse(user.info)


@admin_required
@with_json_params(args=["username"])
async def delete_user(req: Request, username: str) -> Response:
    try:
        user = await User.get(username=username)
    except DoesNotExist:
        return PlainTextResponse(
            f"The user {username} doesn't exist.", status_code=400
        )
    await user.delete()
    logger.info(f"{req.scope['user']} deleted {user}.")
    return PlainTextResponse("")
