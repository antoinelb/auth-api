from typing import Any, Dict, Iterator, List, Optional, Tuple, Type

from argon2 import PasswordHasher
from tortoise import BaseDBAsyncClient, fields
from tortoise.models import Model
from tortoise.signals import pre_save


class User(Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(64, unique=True, index=True)
    password = fields.TextField()
    is_admin = fields.BooleanField(default=False)
    pin = fields.CharField(4, null=True)
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    class Meta:
        table = "users"

    def __str__(self: "User") -> str:
        if self.is_admin:
            return f"<admin user {self.username}>"
        else:
            return f"<user {self.username}>"

    def __iter__(self: "User") -> Iterator[Tuple[str, Any]]:
        return (
            (field, val)
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        )

    @property
    def info(self: "User") -> Dict[str, Any]:
        return {
            "username": self.username,
            "is_admin": self.is_admin,
            "pin": self.pin,
            "created_on": self.created_on,
            "last_modified": self.last_modified,
        }


@pre_save(User)
async def user_pre_save(
    sender: Type[User],
    instance: User,
    using_db: Optional[BaseDBAsyncClient],
    update_fields: List[str],
) -> None:
    hasher = PasswordHasher()
    instance.password = hasher.hash(instance.password)
