from typing import Any, Iterator, Tuple

from tortoise import fields
from tortoise.models import Model


class BlacklistedToken(Model):
    id = fields.IntField(pk=True)
    token = fields.CharField(256, unique=True, index=True)
    expiry = fields.DatetimeField()
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    class Meta:
        table = "blacklisted_tokens"

    def __str__(self: "BlacklistedToken") -> str:
        return f"<token {self.id}>"

    def __iter__(self: "BlacklistedToken") -> Iterator[Tuple[str, Any]]:
        return (
            (field, val)
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        )
