from tortoise.exceptions import DoesNotExist, IntegrityError

from .db import User
from .logging import logger


async def update_admin_status(username: str, is_admin: bool) -> bool:
    try:
        user = await User.get(username=username)
    except DoesNotExist:
        return False

    if user.is_admin == is_admin:
        return True

    await user.update_from_dict({"is_admin": is_admin})
    await user.save()

    if is_admin:
        logger.info(f"{user} was granted admin status.")
    else:
        logger.info(f"{user} was revoked admin status.")

    return True


async def create_admin_user(username: str, password: str) -> bool:
    try:
        await User.create(username=username, password=password, is_admin=True)
    except IntegrityError:
        return False
    return True


async def set_password(username: str, password: str) -> None:
    user = await User.get(username=username)
    await user.update_from_dict({"password": password})
    await user.save()
    logger.info(f"Updated password for {user}.")
